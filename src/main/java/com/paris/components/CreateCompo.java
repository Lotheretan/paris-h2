package com.paris.components;

import java.io.IOException;

import io.github.jeemv.springboot.vuejs.components.VueComponent;

// PERMET DE CREE LE BOUTON EN JS
public class CreateCompo {

	public static void main(String[] args) {
		VueComponent compo= new VueComponent("button-confirm");
		compo.setTemplateFile("vueJS/button-counter.html");
		compo.setProps("title","message","OkayCaption","cancelCaption");
		compo.addData("visible2", false);
		try {
			compo.createFile("vueJS/button-confirm.js", true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
