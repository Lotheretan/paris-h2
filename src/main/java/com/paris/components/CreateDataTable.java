package com.paris.components;

import java.io.IOException;

import io.github.jeemv.springboot.vuejs.components.VueComponent;

// PERMET DE CREE LE BOUTON EN JS
public class CreateDataTable {

	public static void main(String[] args) {
		VueComponent compo= new VueComponent("data-table");
		compo.setProps("data","captions");
		compo.addMethod("handleEdit", "", "index","row");
		compo.addMethod("handleDelete", "", "index","row");
		compo.setTemplateFile("vueJS/data-table.html");
		try {
			compo.createFile("vueJS/data-table.js", true);
		} catch (IOException e) {
			// TODO Auto-generated catch blocky
			e.printStackTrace();
		}
	}

}
