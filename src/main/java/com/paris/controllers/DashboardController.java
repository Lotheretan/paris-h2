package com.paris.controllers;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.paris.entities.Action;
import com.paris.entities.Entite;
import com.paris.entities.Evenement;
import com.paris.entities.Friends;
import com.paris.entities.Joueur;
import com.paris.entities.Pari;
import com.paris.repositories.ActionRepository;
import com.paris.repositories.EntiteRepository;
import com.paris.repositories.EvenementRepository;
import com.paris.repositories.FriendsRepository;
import com.paris.repositories.JoueurRepository;
import com.paris.repositories.PariRepository;

import io.github.jeemv.springboot.vuejs.VueJS;
import io.github.jeemv.springboot.vuejs.utilities.Http;

@Controller //Initiallise la classe en tant que Controller
@RequestMapping("home")
public class DashboardController {
	@Value("#{servletContext.contextPath}")
    private String servletContextPath;
	
	@Autowired
	private EvenementRepository EventRepo;
	
	@Autowired
	private JoueurRepository JoueurRepo;
	
	@Autowired
	private PariRepository PariRepo;
	
	@Autowired
	private FriendsRepository AmiRepo;
	
	@Autowired
	 private ActionRepository repoAction;
	
	@Autowired
	 private EntiteRepository repoEntite;
	
	@ModelAttribute("vue")
	public VueJS getVue() {
		return new VueJS("#app");
	}
	
	@GetMapping("")
	public String Index(HttpSession session,ModelMap model,@ModelAttribute("vue") VueJS vue) {
		vue.addData("EventChecked", false);
		vue.addData("PariChecked", false);
		
		
		//Get connected user ID
		if(session.getAttribute("joueur")==null) {
			return "index";
		}
		Joueur j =(Joueur) session.getAttribute("joueur");
		model.put("userId",j.getId());
		
		//List<Friends> friends = AmiRepo.findAll();
		List<Joueur> friends = JoueurRepo.findAll();
		vue.addData("FriendTable", friends);
		
		//New Evenement
		List<Evenement> evenements=EventRepo.findAll();
		model.put("evenements", evenements); // {{#evenements}}
		
		//NewPari
		List<Pari> paris=PariRepo.findAll();
		List<Action> actions=repoAction.findAll();
		List<Entite> entites=repoEntite.findAll();
		model.put("actions", actions);
		model.put("entites", entites);
		model.put("paris", paris);
		
		//vue.addMethod("deleteJoueur", Http.post("joueur/deleteJoueur","{id:row.id}","row.visible = false;"),"row");
		vue.addMethod("open2", "this.$confirm('This will add this user to your friend list. Continue?', 'Warning', {" + 
				"          confirmButtonText: 'OK'," + 
				"          cancelButtonText: 'Cancel'," + 
				"          type: 'warning'" + 
				"        }).then(() => {" + 
				"          this.$message({" + 
				"            type: 'success'," + 
				"            message: 'Friend Added'" + 
				"          });" + 
				"        });");
		
		//MyBets
		List<Pari> mybets=PariRepo.findByJoueur(j);
		model.put("mybets", mybets);
		
			try {
				//MyEvents
				List<Evenement> myEvent = EventRepo.findAllByJoueur(j);
				model.put("myevents", myEvent);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		return "home/dashboard";
	}
	
	@GetMapping("profil")
	public String profil(HttpSession session,ModelMap model,@ModelAttribute("vue") VueJS vue) 
	{
		String validmsg = (String) session.getAttribute("validmsg");
		vue.addData("visiblevalid", false);
		model.put("validmsg", "");
		if(validmsg != null) {
			vue.addData("visiblevalid", true);
			model.put("validmsg", validmsg);
			session.removeAttribute("validmsg");
		}
		vue.addData("sidechoiceInfo", true);
		vue.addData("sidechoiceFriend", false);
		
		Joueur j =(Joueur) session.getAttribute("joueur");
		model.put("nom", j.getNom());
		model.put("prenom", j.getPrenom());
		model.put("age", j.getAge());
		model.put("id", j.getId());
		model.put("password", j.getPassword());
		model.put("email", j.getEmail());
		// Envoie des données du joueur
		
		return ("home/profil");	
	}
}
