package com.paris.controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.view.RedirectView;

import com.paris.entities.Action;
import com.paris.entities.Entite;
import com.paris.entities.Evenement;
import com.paris.entities.Joueur;
import com.paris.repositories.EvenementRepository;

import io.github.jeemv.springboot.vuejs.VueJS;



@Controller //Initiallise la classe en tant que Controller
@RequestMapping("evenements") //Map l'acces avec /evenements

public class EvenementController {
	@Value("#{servletContext.contextPath}")
    private String servletContextPath;

	@Autowired
	private EvenementRepository repo;
	
	@ModelAttribute("vue")
	public VueJS getVue() {
		return new VueJS("#app");
	}
	
	@GetMapping("")
	public String Index(ModelMap model,@ModelAttribute("vue") VueJS vue) {
		List<Evenement> evenements=repo.findAll();
		model.put("evenements", evenements); // {{#evenements}}
		return "evenements/index";
	}
	
	@GetMapping("delete/{id}")
	public RedirectView delete(@PathVariable int id) {
		Optional<Evenement> evenements=repo.findById(id);
		if(evenements.isPresent()) {
			repo.delete(evenements.get());
		}
		return new RedirectView("/evenements");
	}
	
	@PostMapping("addEvenement")
	public RedirectView submitNewEvenement(HttpSession session,HttpServletRequest request, ModelMap model,@ModelAttribute("vue") VueJS vue) throws ParseException {
		String action= request.getParameter("action");
		String entite= request.getParameter("entite");
		String role= request.getParameter("role");
		Date finPari= new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("finPari"));
		double cote = Double.parseDouble(request.getParameter("cote"));
		
		if(role != null) {
			Joueur j =(Joueur) session.getAttribute("joueur");
			if(role.equals("user")) {
				try {
					Evenement evenement = new Evenement(action, entite, j,cote, finPari);
					repo.saveAndFlush(evenement);
				}catch(Exception e){
					e.printStackTrace();
				}
				return new RedirectView("/home");
			}
			else {
				Evenement evenement = new Evenement(action, entite, cote);
				repo.saveAndFlush(evenement);
			}
		}
		return new RedirectView("/evenements");
		
	}
	
	@GetMapping("modif/{id}")  // Pre-remplie les valeurs dans les champs de texte
	public String modif(ModelMap model, @PathVariable(value="id") int id,@ModelAttribute("vue") VueJS vue) {
		Optional<Evenement> evenementId=repo.findById(id); //Permet de recuperer un ID dans la BDD
		Evenement evenement=repo.getOne(id); // Permet d'utiliser les getNom, getPrenom etc...
		if(evenementId.isPresent()) {
			model.put("action", evenement.getAction());
			model.put("entite", evenement.getEntite());
			model.put("cote", evenement.getCote());
			model.put("evenementId", id);
			return "evenements/modifie";
		}
		return "evenement/nope";
	}
	
	@PostMapping("modifEvenement") // Est utiliser lorsque l'on appuie sur le bouton submit
	public RedirectView submitModifEvenement(HttpServletRequest request, ModelMap model,@ModelAttribute("vue") VueJS vue) {
		String action= request.getParameter("action");
		String entite= request.getParameter("entite");
		double cote = Double.parseDouble(request.getParameter("cote"));
		int id = Integer.parseInt(request.getParameter("id"));
		try {
			Optional<Evenement> evenementId=repo.findById(id);
			Evenement evenement=repo.getOne(id);
			if(evenementId.isPresent()) {
				evenement.setAction(new Action(action));
				evenement.setEntite(new Entite(entite));
				evenement.setCote(cote);
				repo.saveAndFlush(evenement);
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return new RedirectView("evenements");
	}
}
