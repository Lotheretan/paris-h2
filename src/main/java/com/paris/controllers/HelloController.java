package com.paris.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("hello")
public class HelloController {
	@Value("#{servletContext.contextPath}")
    private String servletContextPath;

	@RequestMapping("")
	public @ResponseBody String hello() {
		return "Hello World!";
	}
	@GetMapping("Mustache")
	public String helloWithMustache(ModelMap model) {
		model.put("message", "Hello world with Mustache");
		model.put("titre", "Titre de la page");
		return "HelloView";
	}
	@GetMapping("Perso/{aQui}")
	public String helloWho(ModelMap model,@PathVariable(value="aQui") String personne) {
		model.put("message", "Hello "+personne+" world with Mustache");
		model.put("titre", "Titre de la page");
		return "HelloView";
	}
	

}
