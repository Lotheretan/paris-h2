package com.paris.controllers;


import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.view.RedirectView;

import com.paris.entities.Joueur;
import com.paris.repositories.JoueurRepository;

import io.github.jeemv.springboot.vuejs.VueJS;

@Controller //Initiallise la classe en tant que Controller
@RequestMapping("index") //Map l'acces avec /index/
public class HomeController {
	@Value("#{servletContext.contextPath}")
    private String servletContextPath;
	
	@Autowired
	private JoueurRepository repo;
	
	@ModelAttribute("vue")
	public VueJS getVue() {
		return new VueJS("#app");
	}
	
	@GetMapping("")
	public String Index(HttpSession session,ModelMap model,@ModelAttribute("vue") VueJS vue) {
		
		String errormsg = "";
		vue.addData("visible", false);
		model.put("errormsg", "");
		if(session.getAttribute("joueur")!=null) {
			return "home/dashboard";
		}
		else {
			if(session.getAttribute("joueur")==null && session.getAttribute("errormsg")!=null) {
				vue.addData("visible", true);
				errormsg = (String) session.getAttribute("errormsg");
				model.put("errormsg", errormsg);
				session.removeAttribute("errormsg");
			}
		}
		return "home/index";
	}
	
	@GetMapping("register")
	public String SignInPage(ModelMap model,@ModelAttribute("vue") VueJS vue) {
		return "home/signin";
	}
	
	@PostMapping("login")
	@ResponseBody
	public RedirectView login(HttpSession session,HttpServletRequest request) {
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		try {
			Joueur joueur=repo.findByEmail(email);
			int id = joueur.getId();
			Optional<Joueur> joueurById=repo.findById(id); //Permet de recuperer un joueur par son ID dans la BDD
			if(joueurById.isPresent()) {
				if(joueur.getPassword().equals(password)) {
					session.setAttribute("joueur", joueur);
					return new RedirectView("/home");
				}
				session.setAttribute("errormsg", "Wrong password");
				return new RedirectView("");
				
			}
		}catch(Exception e){
			session.setAttribute("errormsg", "This user doesn't exist, please check the email address");
			return new RedirectView("");
		}
		return null;
		
	}
	
	@PostMapping("signIn")
	public RedirectView signIn(HttpServletRequest request) {
		String nom= request.getParameter("nom");
		String prenom= request.getParameter("prenom");
		int age = Integer.parseInt(request.getParameter("age"));
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		try {
			Joueur joueur = new Joueur(nom,prenom,age,email,password);
			repo.saveAndFlush(joueur);
		}catch(Exception e){
			e.printStackTrace();
		}
		return new RedirectView("/index");
	}
	
	@GetMapping("logout")
	public RedirectView logout(HttpSession session) 
	{
		session.invalidate();
		return new RedirectView("/home");
	}
	
}
