package com.paris.controllers;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.view.RedirectView;


import com.paris.entities.Joueur;
import com.paris.repositories.JoueurRepository;
import io.github.jeemv.springboot.vuejs.VueJS;
import io.github.jeemv.springboot.vuejs.utilities.Http;

@Controller //Initiallise la classe en tant que Controller
@RequestMapping("/joueur") //Map l'acces avec /joueur/
public class JoueurController {
	@Value("#{servletContext.contextPath}")
    private String servletContextPath;
	

	@Autowired
	private JoueurRepository repo;
	
	@ModelAttribute("vue")
	public VueJS getVue() {
		return new VueJS("#app");
	}

	@GetMapping("")
	public String Index(ModelMap model,@ModelAttribute("vue") VueJS vue) {
		List<Joueur> joueurs=repo.findAll();
		vue.addData("joueurs", joueurs);
		vue.addMethod("submitModifJoueur", Http.submitForm("formModif",
				"scope.row.visible = false;"),"row");
		vue.addMethod("deleteJoueur", Http.post("joueur/deleteJoueur","{id:row.id}",
				"row.visible = false;"),"row");
		return "joueur/index";
	}
	
	@PostMapping("deleteJoueur")
	@ResponseBody
	public RedirectView deleteJoueur(@RequestBody Joueur joueur) {
		Optional<Joueur> joueurs=repo.findById(joueur.getId());
			repo.delete(joueurs.get());
		return new RedirectView("joueur");
	}
	
	@PostMapping("addJoueur")
	public RedirectView submitNewJoueur(HttpServletRequest request) {
		String nom= request.getParameter("nom");
		String prenom= request.getParameter("prenom");
		int age = Integer.parseInt(request.getParameter("age"));
		try {
			Joueur joueur = new Joueur(nom,prenom,age);
			repo.saveAndFlush(joueur);
		}catch(Exception e){
			e.printStackTrace();
		}
		return new RedirectView("joueur");
	}
	
	
	@GetMapping("modif/{id}")  // Pre-remplie les valeurs dans les champs de texte
	public String modif(ModelMap model, @PathVariable(value="id") int id,@ModelAttribute("vue") VueJS vue) {
		Optional<Joueur> joueurId=repo.findById(id); //Permet de recuperer un joueur par son ID dans la BDD
		Joueur joueur=repo.getOne(id); // Permet d'utiliser les getNom, getPrenom etc...
		if(joueurId.isPresent()) {
			model.put("nom", joueur.getNom());
			model.put("prenom", joueur.getPrenom());
			model.put("age", joueur.getAge());
			model.put("joueurId", id);
			return "joueur/modifie";
		}
		return "joueur/nope";
	}
	
	@PostMapping("submitModifJoueur") // Est utiliser lorsque l'on appuie sur le bouton submit
	@ResponseBody
	public RedirectView submitModifJoueur(@RequestBody Joueur joueur) {
		Optional<Joueur> joueurs=repo.findById(joueur.getId());
		repo.saveAndFlush(joueurs.get());
		return new RedirectView("joueur");
	}
	
	@PostMapping("modifJoueur")
	public RedirectView submitModifJoueur2(ModelMap model,HttpSession session,HttpServletRequest request,@ModelAttribute("vue") VueJS vue) {
		int id = Integer.parseInt(request.getParameter("id"));
		int age = Integer.parseInt(request.getParameter("age"));
		String role= request.getParameter("role");
		try {
			Optional<Joueur> joueurId=repo.findById(id);
			Joueur joueur=repo.getOne(id);
			if(joueurId.isPresent()) {
				joueur.setNom(request.getParameter("nom"));
				joueur.setPrenom(request.getParameter("prenom"));
				joueur.setEmail(request.getParameter("email"));
				joueur.setPassword(request.getParameter("password"));
				joueur.setAge(age);
				repo.saveAndFlush(joueur);
				if(role != null) {
					if(role.equals("user")) {
						session.setAttribute("joueur", joueur);
						session.setAttribute("validmsg", "Joueur modifié");
						return new RedirectView("/home/profil");
					}
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return new RedirectView("joueur");
	}
	
}
