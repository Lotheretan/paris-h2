package com.paris.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.view.RedirectView;


import com.paris.entities.Evenement;
import com.paris.entities.Joueur;

import com.paris.entities.Pari;
import com.paris.entities.PariCombine;
import com.paris.entities.PariSimple;
import com.paris.repositories.PariRepository;
import io.github.jeemv.springboot.vuejs.VueJS;

import com.paris.repositories.EvenementRepository;
import com.paris.repositories.JoueurRepository;

@Controller
@RequestMapping("pari")
public class PariController {
	@Value("#{servletContext.contextPath}")
    private String servletContextPath;
	
	@Autowired
	 private PariRepository repo;
	
	@Autowired
	 private JoueurRepository repoJoueurs;
	
	//@Autowired
	//private ActionRepository repoAction;
	
	//@Autowired
	 //private EntiteRepository repoEntite;
	
	@Autowired
	private EvenementRepository repoEvent;
	
	@ModelAttribute("vue")
	public VueJS getVue() {
		return new VueJS("#app");
	}
	 
	@RequestMapping("")
	public String all(ModelMap model) {
		List<Pari> paris=repo.findAll();
		List<Joueur> joueurs=repoJoueurs.findAll();
		List<Evenement> evenements=repoEvent.findAll();
		//List<Action> actions=repoAction.findAll();
		//List<Entite> entites=repoEntite.findAll();
		//model.put("action", actions);
		//model.put("entite", entites);
		model.put("events", evenements); 
		model.put("pari", paris);
		model.put("joueurs", joueurs);
		VueJS vue=new VueJS("#app");
		model.put("vueJS", vue.getScript());
		return "pari/index";
		
	}
	
	@PostMapping("addPariSimple")
	public RedirectView submitNewPariSimple(HttpServletRequest request, @ModelAttribute("vue") VueJS vue) {
		int idjoueur = Integer.parseInt(request.getParameter("idjoueur"));
		//int actionID= Integer.parseInt(request.getParameter("action"));
		//int entiteID= Integer.parseInt(request.getParameter("entite"));
		int eventID= Integer.parseInt(request.getParameter("idevent"));
		String role= request.getParameter("role");
		int mise = Integer.parseInt(request.getParameter("mise"));
		try {
			Joueur joueur=repoJoueurs.getOne(idjoueur);
			//Pari ps=new PariSimple(joueur, new Evenement(action, entite), mise);
			Evenement event = repoEvent.getOne(eventID);
			Pari ps=new PariSimple(joueur, event, mise);
			repo.saveAndFlush(ps);
		}catch(Exception e) {
			e.printStackTrace();
		}
		if(role != null) {
				return new RedirectView("home");
		}
		return new RedirectView("pari");
	}
	
	@PostMapping("addNewCombine")
	public RedirectView SubmitNewCombine(HttpServletRequest request,@ModelAttribute("vue") VueJS vue) {
		int idjoueur = Integer.parseInt(request.getParameter("idjoueur"));
		String action= request.getParameter("action");
		String entite= request.getParameter("entite");
		int mise = Integer.parseInt(request.getParameter("mise"));
		//int number = Integer.parseInt(request.getParameter("number"));
		try {
			Joueur joueur=repoJoueurs.getOne(idjoueur);
			PariCombine pc = new PariCombine(joueur, mise);
			pc.ajouterEvenements(
					new Evenement(action, entite, mise)
					);					
			repo.saveAndFlush(pc);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return new RedirectView("pari");
	}

}
