package com.paris.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.paris.entities.Joueur;
import com.paris.repositories.JoueurRepository;
import io.github.jeemv.springboot.vuejs.VueJS;
import io.github.jeemv.springboot.vuejs.utilities.Http;

@Controller
@RequestMapping("ui")
public class UiController {
	@Value("#{servletContext.contextPath}")
    private String servletContextPath;
	
	@Autowired
	private JoueurRepository repoJoueur;
	
	@GetMapping("test")
	public String hello() 
	{
		return "ui/hello";
	}
	
	@ModelAttribute("vue")
	public VueJS getVue() {
		return new VueJS("#app");
	}
	
	@GetMapping("test2")
	public String test2(@ModelAttribute("vue") VueJS vue) 
	{
		vue.addData("buttonText", "Button Okay");
		vue.addData("visible", false);
		vue.addData("progress", 1);
		vue.addData("pbColor", "white");
		vue.addMethod("notification",
				"const h = this.$createElement;" +  
				"        this.$notify({" + 
				"          title: 'Title'," + 
				"          message: h('i', { style: 'color: teal' }, 'La nouvelle valeur est '+this.progress)" + 
				"});"+
				"this.pbColor=(this.progress>50)?'lightgreen':'orange';");
		vue.addComputed("count", "return this.users.length");
		vue.addMethod("handleOkay", "console.log('okay');");
		vue.addMethod("handleCancel", "console.log('cancel');");
		vue.addData("captions", new String[] {"nom","prenom","age"});
		return "ui/test";
	}
	
	@GetMapping("joueurs")
	public String listJoueurs(@ModelAttribute("vue") VueJS vue) {
		List<Joueur> joueurs=repoJoueur.findAll();
		vue.addData("joueurs", joueurs);
		try {
			vue.addData("joueurActif", new Joueur());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "ui/joueurs";
	}
	
	@GetMapping("form")
	public String form(@ModelAttribute("vue") VueJS vue) throws Exception {
		vue.addData("joueur", new Joueur());
		vue.addData("formVisible", true);
		vue.addMethod("submit", Http.submitForm("formJoueur",
				"this.message=this.joueur.nom+ ' enregistré dans la base.';"
				+ "	this.formVisible=false;"));
		return "ui/form";
	}
	
	@PostMapping("submit")
	@ResponseBody
	public Joueur submit(@RequestBody Joueur joueur) throws Exception {
		repoJoueur.saveAndFlush(joueur);
		return new Joueur();
	}
	
	@GetMapping ("dataTable")
	public String testDataTable(@ModelAttribute("vue") VueJS vue) {
		List<Joueur> joueurs=repoJoueur.findAll();
		vue.addData("joueurs", joueurs);
		vue.addData("captions", new String[] {"nom","prenom","age"});
		return "ui/dataTable";
	}
}
