package com.paris.entities;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import paris.exception.edu.NotRealisedEventException;

@Entity
public class Evenement {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@ManyToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name="idAction")
	private Action action;
	
	@ManyToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name="idEntite")
	private Entite entite;
	
	@ManyToOne(cascade=CascadeType.DETACH)
	@JoinColumn(name="idJoueur")
	private Joueur joueur;	
	
	@Column
	private double cote;
	@Column
	private String type;
	@Column
	private boolean realise;
	@Column
	private Date date;
	@Column
	private Date finPari;
	
	public Date getFinPari() {
		return finPari;
	}

	public void setFinPari(Date finPari) {
		this.finPari = finPari;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	public Evenement() {
		this("","");
	}
	
	public Evenement(Action action, Entite entite) {
		this(action,entite,1);
	}

	public Evenement(Action action, Entite entite, double cote) {
		this.action = action;
		this.entite = entite;
		this.cote=cote;
		this.realise=false;
		date=new Date();
	}
	
	public Evenement(Action action, Entite entite,Joueur joueur, double cote, Date finPari) {
		this.action = action;
		this.entite = entite;
		this.cote=cote;
		this.joueur=joueur;
		this.realise=false;
		this.finPari=finPari;
		date=new Date();
	}
	
	public Evenement(String action, String entite, double cote) {
		this(new Action(action),new Entite(entite), cote);
	}
	
	public Evenement(String action, String entite,Joueur joueur, double cote,Date finPari) {
		this(new Action(action),new Entite(entite), joueur,cote, finPari);
	}
	
	public Evenement(String action, String entite) {
		this(new Action(action),new Entite(entite),1);
	}

	@Override
	public String toString() {
		String strRealise="en attente de realisation..";
		if(estPasse())
		{
			strRealise=realise?"Realiser":"Non realiser";
		}
		return entite.toString()+" "+action.toString()+" (cote : "+cote+") "+strRealise;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Action getAction() {
		return action;
	}
	
	public void setAction(Action action) {
		this.action = action;
	}

	public Entite getEntite() {
		return entite;
	}
	
	public void setEntite(Entite entite) {
		this.entite = entite;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Joueur getJoueur() {
		return joueur;
	}

	public void setJoueur(Joueur joueur) {
		this.joueur = joueur;
	}

	public boolean estRealise() {
		return realise && estPasse();
	}

	public void setRealise(boolean realise) throws NotRealisedEventException{
		if(!estPasse()) {
			throw new NotRealisedEventException();
		}
		this.realise = realise;
	}

	public double getCote() {
		return cote;
	}

	public Evenement setCote(double cote) {
		this.cote = cote;
		return this;
	}
	
	public boolean estPasse() {
		Date now=new Date();
		return date.before(now);
	}
	
	
}
