package com.paris.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="Friends")
public class Friends {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@ManyToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name="idJoueur1")
	private Joueur joueur1;	
	
	@ManyToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name="idJoueur2")
	private Joueur joueur2;	
	
	@Column
	private boolean demand;
	
	@Column
	private boolean accept;
	
	public Friends() {
		this("pas de joueur","pas de joueur",false,false);
	}
	
	public Friends(String joueur1,String joueur2, boolean demand, boolean accept) {
		
	}
	
	public Friends(Joueur joueur1,Joueur joueur2) {
		this.joueur1 = joueur1;
		this.joueur2 = joueur2;
	}
	
	public Friends(Joueur joueur1,Joueur joueur2, boolean demand, boolean accept) {
		this.joueur1 = joueur1;
		this.joueur2 = joueur2;
		this.demand = demand;
		this.accept = accept;
	}
	
	public boolean isDemand() {
		return demand;
	}

	public void setDemand(boolean demand) {
		this.demand = demand;
	}

	public boolean isAccept() {
		return accept;
	}

	public void setAccept(boolean accept) {
		this.accept = accept;
	}

	public Joueur getJoueur1() {
		return joueur1;
	}

	public void setJoueur1(Joueur joueur1) {
		this.joueur1 = joueur1;
	}

	public Joueur getJoueur2() {
		return joueur2;
	}

	public void setJoueur2(Joueur joueur2) {
		this.joueur2 = joueur2;
	}
	
	@Override
	public String toString() {
		return this.joueur1+" est ami avec "+this.joueur2;
	}
}
