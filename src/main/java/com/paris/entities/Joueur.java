package com.paris.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Joueur")
public class Joueur {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name="nom")//indique que le nom du champ dans la base de donnée est "nom" pour cette variable
	private String nom;
	@Column(name="prenom")
	private String prenom;
	@Column(name="age")
	private int age;
	@Column(name="email")
	private String email;
	@Column(name="password")
	private String password;
	
	public Joueur(String nom, String prenom,int age, String email, String password) throws Exception {
		this.nom = nom;
		this.prenom = prenom;
		setAge(age);
		this.password= password;
		this.email = email;
	}

	public Joueur(String nom, String prenom,int age) throws Exception {
		this.nom = nom;
		this.prenom = prenom;
		setAge(age);
	}
	
	public Joueur(String nom, String prenom) throws Exception {
		this(nom,prenom,18);
	}
	
	public Joueur() throws Exception {
		this("pas de nom","pas de prenom",18);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) throws Exception {
			if (age>=18) {
				this.age = age;
			}
			else {
				throw new Exception("Vous n'avez pas l'age legal !");
			}
		}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void afficher() {
		System.out.println(this); 
	}
	
	public static void afficher(Joueur unJoueur) {
	unJoueur.afficher();
	}
	
	@Override
	public String toString() {
		return prenom+" "+this.nom;
	}
}
