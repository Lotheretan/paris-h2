package com.paris.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
public abstract class Pari {
	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	private int id;
	
	@Column
	private Date date;	
	
	@ManyToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name="idJoueur")
	private Joueur joueur;	
	@Column
	protected float mise;
	
	public Pari(Joueur joueur,float mise) {
		this.joueur=joueur;
		this.mise=mise;
		date=new Date();
	}
	
	public abstract boolean estGagne();
	public abstract double gains(); 
	
	public abstract List<Evenement> getEvenements();
	
	public abstract String getType();
	
	public String toString() {
		return "Pari du "+date+"\nJoueur : "+joueur;
	}
}
