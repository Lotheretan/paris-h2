package com.paris.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

@Entity
public class PariCombine extends Pari {
	@OneToMany(cascade=CascadeType.PERSIST)
	protected List<Evenement> evenements;
	
	public PariCombine() {
		this(null,0);
	}
	
	public PariCombine(Joueur joueur, float mise) {
		super(joueur, mise);
		evenements=new ArrayList<>();
	}

	public void ajouterEvenements(Evenement...evenements) {
		for(Evenement event:evenements) {
			this.evenements.add(event);
		}
	}
	
	@Override
	public boolean estGagne(){
		for(Evenement eve:evenements) {
			if(!eve.estRealise()) {
				return false;
			}
		}
		return true;
	}

	@Override
	public double gains() {
		float totCotes=0;
			if(estGagne()) {
				for(Evenement eve:evenements) {
					totCotes+=eve.getCote();
				}
			}
		return totCotes*mise;
	}

	public void clearEvenements() {
		evenements.clear();
	}
	
	public int nbEvenements() {
		return evenements.size();
	}

	@Override
	public List<Evenement> getEvenements() {
		return evenements;
	}

	@Override
	public String getType() {
		return "Combine";
	}
}
