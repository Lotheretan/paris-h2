package com.paris.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class PariSimple extends Pari {
	
	@ManyToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name="idEvenement")
	protected Evenement evenement;
	
	public PariSimple(Joueur joueur, Evenement evenement, float mise) {
		super(joueur, mise);
		this.evenement=evenement;
	}
	
	public PariSimple() {
		this(null,null,0);
	}

	public boolean estGagne(){
		return evenement.estRealise();		
	}
	
	public double gains() {
		double gains=0;
		if(evenement.estPasse()) {
			if(evenement.estRealise()) {
				gains=evenement.getCote()*mise;
			}
			return gains;
		}
		return gains;
	}

	@Override
	public List<Evenement> getEvenements() {
		List<Evenement> events=new ArrayList<>();
		events.add(this.evenement);
		return events;
	}

	@Override
	public String getType() {
		return "Simple";
	}
}
