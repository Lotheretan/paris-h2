package com.paris.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.paris.entities.Action;

public interface ActionRepository extends JpaRepository<Action, Integer> {

}
