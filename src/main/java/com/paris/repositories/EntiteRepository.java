package com.paris.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.paris.entities.Entite;

public interface EntiteRepository extends JpaRepository<Entite, Integer> {

}
