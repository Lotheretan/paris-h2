package com.paris.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.paris.entities.Evenement;
import com.paris.entities.Joueur;

@Repository
public interface EvenementRepository extends JpaRepository<Evenement, Integer> {

	List<Evenement> findAllByJoueur(Joueur j);

}

