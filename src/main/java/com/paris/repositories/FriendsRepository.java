package com.paris.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.paris.entities.Friends;
import com.paris.entities.Joueur;

public interface FriendsRepository extends JpaRepository<Friends, Integer> {
		Friends findByJoueur1AndJoueur2(Joueur joueur1,Joueur joueur2);
}
