package com.paris.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.paris.entities.Joueur;

@Repository
public interface JoueurRepository extends JpaRepository<Joueur, Integer> {
	Joueur findByEmail(String email);
	

}
