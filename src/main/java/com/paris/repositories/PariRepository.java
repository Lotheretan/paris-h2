package com.paris.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.paris.entities.Joueur;
import com.paris.entities.Pari;
@Repository
public interface PariRepository extends JpaRepository<Pari, Integer> {
	List<Pari> findByJoueur(Joueur joueur);
}
