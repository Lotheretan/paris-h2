package paris.exception.edu;

public class NotRealisedEventException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NotRealisedEventException() {
		// TODO Auto-generated constructor stub
	}

	public NotRealisedEventException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public NotRealisedEventException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public NotRealisedEventException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public NotRealisedEventException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public String getMessage() {
		return "�v�nement non r�alis�";
	}

}
