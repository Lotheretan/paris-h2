//Script generated with VueComponent at Thu Oct 11 09:15:27 CEST 2018
Vue.component('button-counter',{
	"props":{
		caption:{
			"type":[String],"required":true
			}
		,type:{
			"type":[String]
			}
		}
	,"data":function() {
		 return {
			"count":0
			}
		;
		}
	,"template":"<el-button :type='type' @click='count++'>{{caption}} : {{count}} <el-tag type=\"danger\"><slot></slot></el-tag></el-button>"
	}
);